package sda.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import sda.spring.security.AppUserDetailsManager;
import sda.spring.security.PlainTextPasswordEncoder;
import sda.spring.security.User;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Bean
	public PasswordEncoder passwordEncoder(){
		// using BCrypt encoder
		// return new BCryptPasswordEncoder();
		
		return new PlainTextPasswordEncoder();
	}
	
	@Bean
	public UserDetailsManager userDetailsManager(){
		AppUserDetailsManager manager = new AppUserDetailsManager();
		//Get users from database
		manager.createUser(new User("daniel", "12345"));
		
		return manager;
		
		// Example of using in memory user manager
//		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//		UserDetails user = User.withUsername("daniel").password("12345").authorities("ADMIN").build();
//		
//		manager.createUser(user);
//		return manager;		
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.httpBasic(); // how we secure ? -> HTTP Basic Authentication 
		//http.authorizeRequests().anyRequest().permitAll();
		http.authorizeRequests().anyRequest().authenticated(); // what we secure ? - Any request
	}
}

