package sda.spring.security;

public class PlainTextPasswordEncoder implements org.springframework.security.crypto.password.PasswordEncoder{

	@Override
	public String encode(CharSequence rawPassword) {
		
		return rawPassword.toString();
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {

		return rawPassword.toString().equals(encodedPassword);
	}

}
